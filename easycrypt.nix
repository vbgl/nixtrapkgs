self: super: {

easycrypt-unstable =
let rev = "b13fb54e49840bd81ed3d872883c212cc6bab273"; in
let inherit (super) why3; in
let why3 = super.why3.overrideAttrs (o: {
  name = "why3-1.4.0";
  src = super.fetchurl {
    url = "https://gforge.inria.fr/frs/download.php/file/38425/why3-1.4.0.tar.gz";
    sha256 = "0lw0cpx347zz9vvwqibmbxgs80fsd16scgk3isscvwxnajpc3rv8";
  };
}); in

super.stdenv.mkDerivation rec {
  version = "1.0-git-20220315";
  pname = "easycrypt";
  src = super.fetchFromGitHub {
    owner = "EasyCrypt";
    repo = "easycrypt";
    inherit rev;
    sha256 = "sha256:10kcpmfkhmws4al3shba8sr8m67fqzf9sklcbxqlibprs8ipk459";
  };

  buildInputs = [ ]
  ++ (with super.ocaml-ng.ocamlPackages; [
    ocaml findlib dune_2
    batteries inifiles dune-build-info menhir menhirLib yojson zarith
  ]);
  propagatedBuildInputs = [ why3 ];

  preConfigure = ''
    substituteInPlace dune-project --replace '(name easycrypt)' '(name easycrypt)(version ${rev})'
  '';

  installPhase = ''
    runHook preInstall
    dune install --prefix $out ${pname}
    rm $out/bin/ec-runtest
    runHook postInstall
  '';

  meta = with super; {
    license = with lib.licenses; [ cecill-c /* program */ cecill-b /* libraries */ ];
    platforms = lib.platforms.all;
    maintainers = [ lib.maintainers.vbgl ];
    homepage = https://www.easycrypt.info/;
    description = "Computer-Aided Cryptographic Proofs";
  };
};

easycrypt-runtest-unstable =
super.python3Packages.buildPythonApplication rec {
  inherit (self.easycrypt-unstable) src version;
  pname = "easycrypt-runtest";

  dontConfigure = true;
  dontBuild = true;
  doCheck = false;

  pythonPath = with super.python3Packages; [ pyyaml ];

  installPhase = ''
    mkdir -p $out/bin
    cp scripts/testing/runtest $out/bin/ec-runtest
  '';
};

}
