self: super: {

patoline =
with super;
let ocamlPackages = ocaml-ng.ocamlPackages_4_07; in
let inherit (ocamlPackages) buildDunePackage; in

let earley = ocamlPackages.earley.overrideAttrs (o: {
  name = "ocaml${ocamlPackages.ocaml.version}-earley-2.0.0";
  src = fetchFromGitHub {
    owner = "rlepigre";
    repo = "ocaml-earley";
    rev = "2.0.0";
    sha256 = "18k7bi7krc4bvqnhijz1q0pfr0nfahghfjifci8rh1q4i5zd0xz5";
  };
}); in

buildDunePackage {
  version = "20190825";
  pname = "patoline";

  useDune2 = true;

  src = fetchzip {
    url = "https://github.com/patoline/patoline/archive/f02816e764356630a8cf11030e6aa73aff125c86.tar.gz";
    sha256 = "1nldzk0ncwdfsmkss2arnsxpv4zfzic66pr07ijbdqjd75yvfilb";
  };

  patches = [ (fetchpatch {
    url = "https://github.com/patoline/patoline/commit/131cb2a9ef3411d5bcbf8b5b22c64a9ec99209c0.patch";
    sha256 = "1ky1p5xrvhf76qqaccaxzrgdhng35ysi3ymadkci1x72qy9nxawp";
  }) ];

  # Should these be propagated too?
  buildInputs = [ ]
  ++ (with ocamlPackages; [ cryptokit dune ])
  ;

  propagatedBuildInputs = [ earley ]
  ++ (with ocamlPackages; [ ocaml findlib camlzip fontconfig imagelib-unix ocaml_sqlite3 ])
  ;

  meta = {
    homepage = "http://patoline.org/";
    description = "A modern digital typesetting system";
    license = stdenv.lib.licenses.gpl2;
  };

};

}
