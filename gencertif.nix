self: super:

{

gencertif =
let inherit (super) stdenv ecm gmp coqPackages ocamlPackages; in
let inherit (coqPackages) coqprime; in
stdenv.mkDerivation rec {
  inherit (coqprime) version src;
  name = "gencertif-${version}";

  sourceRoot = "source/gencertif";

  buildInputs = [ gmp ecm ]
  ++ (with ocamlPackages; [ ocaml findlib num ])
  ;

  postPatch = ''
    substituteInPlace Makefile --replace 'CC=gcc' "" \
    --replace 'ocamlc -o o2v nums.cma str.cma parser.ml' \
    'ocamlfind opt -o o2v -package num,str -linkpkg parser.ml'
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp firstprimes o2v pocklington $out/bin/
  '';

  meta = {
    description = "A C program that generates Pocklington certificates";
    license = stdenv.lib.licenses.lgpl21;
    inherit (coqprime.meta) homepage platforms;
  };
};

}
