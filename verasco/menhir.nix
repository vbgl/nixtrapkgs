{ stdenv, fetchurl, ocaml, findlib, ocamlbuild }:

stdenv.mkDerivation rec {
  pname = "ocaml${ocaml.version}-menhir";
  version = "20170712";

  src = fetchurl {
    url = "http://gallium.inria.fr/~fpottier/menhir/menhir-${version}.tar.gz";
    sha256 = "006hq3bwj81j67f2k9cgzj5wr4hai8j36925p5n3sd2j01ljsj6a";
  };

  buildInputs = [ ocaml findlib ocamlbuild ];

  createFindlibDestdir = true;

  preBuild = ''
    export PREFIX=$out
  '';
}
