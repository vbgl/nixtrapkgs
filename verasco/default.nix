self: super: {

coq_84 =
let inherit (super) callPackage; in
callPackage ./8.4.nix {
  inherit (super.ocaml-ng.ocamlPackages_4_02) ocaml findlib camlp5;
};


menhir_2017 =
let inherit (super) callPackage ocaml-ng; in
callPackage ./menhir.nix {
  inherit (ocaml-ng.ocamlPackages_4_05) ocaml findlib ocamlbuild;
};

verasco =
let inherit (super) callPackage fetchurl; in
callPackage ./verasco.nix {
  coq = self.coq_84;
  inherit (super.ocaml-ng.ocamlPackages_4_05) ocaml findlib zarith;
  menhir = self.menhir_2017;

};

}
