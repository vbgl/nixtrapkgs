super: self: {

proofgeneral =
  super.lib.overrideDerivation
    super.emacs29.pkgs.proof-general (o: {
    name = "ProofGeneral-unstable-2024-01-28";
    src = super.fetchFromGitHub {
      owner = "ProofGeneral";
      repo = "PG";
      rev = "a38857a6a099d0d94184a50093ea6ad331c5c52e";
      hash = "sha256-sUFSHh2PDSn0rcauWS+x4re0o452vMwPC/agLH2muPE=";
    };
    patches = (o.patches or []) ++ [ ./pg-syntax.patch ];

    preConfigure = ''
      substituteInPlace generic/proof-site.el \
        --replace '(qrhl "qRHL" "qrhl")' '(qrhl "qRHL" "qrhl")(squirrel "squirrel" "sp")'
    '' + #FIXME: the following does not work
    ''
      mkdir -p squirrel
      cp ${super.squirrel.src}/utils/*.el squirrel/
      substituteInPlace Makefile --replace qrhl 'qrhl squirrel'
    '';

    postInstall = ''
      DST=$(ls -d1 $out/share/emacs/site-lisp/elpa/proof-general-*)/squirrel
      mkdir -p $DST
      cp ${super.squirrel.src}/utils/*.el $DST/
    '';

  });

}
