self: super: {

dead-code-analyzer =
let inherit (super) stdenv fetchFromGitHub opaline; in
let ocamlPackages = super.ocaml-ng.ocamlPackages_4_08; in

stdenv.mkDerivation rec {
  name = "dead-code-analyzer-20180625";
  src = fetchFromGitHub {
    owner = "vbgl";#LexiFi";
    repo = "dead_code_analyzer";
    rev = "9c51d1e1a831cb33a459f92573da016643b7522b";
    sha256 = "0s4cygrsmc77xk0pkpfr58wglghhzz82sn0pnqzb4fiykprww31m";
  };

  nativeBuildInputs = [ opaline ];
  buildInputs = with ocamlPackages; [ ocaml findlib ];

  buildFlags = [ "all" "opt" "man" ];

  doCheck = false;

  installPhase = "opaline -prefix $out";

  meta = {
    description = "Dead-code analyzer for OCaml";
    inherit (src.meta) homepage;
    inherit (ocamlPackages.ocaml.meta) platforms;
    license = stdenv.lib.licenses.mit;
    maintainers = [ stdenv.lib.maintainers.vbgl ];
  };

};

}
