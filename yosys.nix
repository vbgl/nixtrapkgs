_: pkgs: {

yosys_0_7 =
let inherit (pkgs)
  stdenv fetchurl gawk gcc gnused readline pkgconfig tcl bison flex python3 libffi; in

stdenv.mkDerivation {
  name = "yosys-0.7";
  src = fetchurl {
    url = "https://github.com/cliffordwolf/yosys/archive/yosys-0.7.tar.gz";
    sha256 = "0vkfdn4phvkjqlnpqlr6q5f97bgjc3312vj5jf0vf85zqv88dy9x";
  };

  buildInputs = [ gawk gcc gnused pkgconfig tcl bison flex readline python3 libffi ];

  configurePhase = "make config-gcc";

  enableParallelBuilding = true;

  makeFlags = [ "ENABLE_ABC=0" ];

  installFlags = [ "PREFIX=$(out)" ];

  meta = {
    description = "Yosys Open SYnthesis Suite";
    homepage = "http://www.clifford.at/yosys/";
    license = stdenv.lib.licenses.isc;
    platforms = stdenv.lib.platforms.unix;
  };

};

}
