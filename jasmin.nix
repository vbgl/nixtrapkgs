self: super:

let inherit (super)
  stdenv lib fetchFromGitHub mpfr ppl dune_2
; in
let coqPackages = super.coqPackages_8_14; in
let inherit (coqPackages) coq; in

{

coqword =
let rev = "e39e96d51aa96f386222fd1b38776f2117f325c5"; in
stdenv.mkDerivation rec {
  version = "1.0-git-${builtins.substring 0 8 rev}";
  name = "coq${coq.coq-version}-coqword-${version}";
  src = fetchFromGitHub {
    owner = "jasmin-lang";
    repo = "coqword";
    inherit rev;
    sha256 = "sha256:0703m97rnivcbc7vvbd9rl2dxs6l8n52cbykynw61c6w9rhxspcg";
  };

  buildInputs = [ coq coq.ocamlPackages.ocaml dune_2 ];
  propagatedBuildInputs = [ (coqPackages.mathcomp.override { version = "1.12.0"; }).algebra ];

  buildPhase = ''
    runHook preBuild
    dune build -p coq-mathcomp-word
    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall
    dune install --prefix=$out
    mkdir -p $out/lib/coq/${coq.coq-version}/
    mv $out/lib/coq/user-contrib $out/lib/coq/${coq.coq-version}/
    runHook postInstall
  '';

  meta = {
    description = "Yet Another Coq Library on Machine Words";
    license = lib.licenses.cecill-b;
    inherit (src.meta) homepage;
    inherit (coq.meta) platforms;
  };

};

jasmin =
stdenv.mkDerivation rec {
  name = "jasmin-20220307";
  src = fetchFromGitHub {
    owner = "jasmin-lang";
    repo = "jasmin";
    rev = "960a34d83dc885dbf25de4aa39eac8efbe91ad74";
    sha256 = "sha256:1byzksqz1jxln05yhc2ml2izfdsr7vlcb6kmwjlv551zpkwqzjiz";
  };

  buildInputs = [ coq mpfr ppl ]
  ++ (with coq.ocamlPackages; [
    ocaml findlib ocamlbuild batteries menhir menhirLib zarith apron yojson
  ])
  ;
  propagatedBuildInputs = [ self.coqword ];

  buildPhase = ''
    runHook preBuild
    make -j $NIX_BUILD_CORES -C proofs
    make -C compiler CIL build
    runHook postBuild
  '';

  installFlags = [
    "PREFIX=$(out)"
    "COQLIB=$(out)/lib/coq/${coq.coq-version}/"
  ];

  meta = {
    description = "The Jasmin compiler";
    license = lib.licenses.cecill-b;
    inherit (src.meta) homepage;
    inherit (coq.meta) platforms;
  };

};

}
