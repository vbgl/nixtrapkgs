self: super: {

maskverif =
let inherit (super) fetchFromGitLab ocamlPackages; in
let inherit (ocamlPackages) buildDunePackage
  batteries menhir menhirLib ocamlgraph zarith; in
buildDunePackage {
  pname = "maskverif";
  version = "2020-07-13";
  src = fetchFromGitLab {
    owner = "benjgregoire";
    repo = "maskverif";
    rev = "d25612bfe788c45affa4091f51a81634d14f1a17";
    hash = "sha256-oa80DO84K5bHG3ossE7lQFwbICheT2zfXdUsPuj8f98=";
  };
  nativeBuildInputs = [ menhir ];
  propagatedBuildInputs = [ batteries menhirLib ocamlgraph zarith ];
};

scverif =
let inherit (super) fetchFromGitHub ocamlPackages; in
let inherit (ocamlPackages) buildDunePackage
  batteries menhir menhirLib ppx_deriving ppx_import re zarith
; in

buildDunePackage {
  pname = "scverif";
  version = "1.1.0";
  src = fetchFromGitHub {
    owner = "scverif";
    repo = "scverif";
    rev = "f62dc56b9ae7b910f0aadfccdb3b5d4cb4f78083";
    hash = "sha256-+QmMIdktwGVwv1xisfJLgc5p6/8dPMN0je90/D/TSZY=";
  };
  nativeBuildInputs = [ menhir ];
  buildInputs = [ batteries self.maskverif ppx_deriving ppx_import menhirLib re zarith ];
};

}
