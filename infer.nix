self: super: {

fbinfer-clang-src = super.fetchurl {
  url = "https://github.com/llvm/llvm-project/releases/download/llvmorg-11.1.0/llvm-project-11.1.0.src.tar.xz";
  hash = "sha256:74d2529159fd118c3eac6f90107b5611bccc6f647fdea104024183e8d5e25831";
};

fbinfer =
let inherit (super) stdenv fetchFromGitHub autoconf automake cmake perl python3 ocaml-ng jdk; in
let ocamlPackages = ocaml-ng.ocamlPackages_4_10; in
stdenv.mkDerivation rec {
  pname = "infer";
  version = "1.1.0";
  src = fetchFromGitHub {
    owner = "facebook";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-kCMThk+04lEJS4kOU0C2FdvRnKyihA6Y8Totc92kaQY=";
  };

  preConfigure = ''
    patchShebangs autogen.sh facebook-clang-plugins/clang/setup.sh facebook-clang-plugins/clang/src/prepare_clang_src.sh
    mkdir -p facebook-clang-plugins/clang/src/download
    cp ${self.fbinfer-clang-src} facebook-clang-plugins/clang/src/download/llvm-project.src.tar.xz
    ./autogen.sh
  '';

  postConfigure = ''
    facebook-clang-plugins/clang/src/prepare_clang_src.sh
  '';

  dontUseCmakeConfigure = true;
  #buildTargets = [ "jobs" ];

  nativeBuildInputs = [ autoconf automake cmake perl
    python3
  ]
  ++ (with ocamlPackages; [ ocaml findlib ocamlbuild menhir ])
  ;

  buildInputs = with ocamlPackages; [
    atdgen
    camlzip
    ounit
    sawja

    jdk
  ];
}
;

}
