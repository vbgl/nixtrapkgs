self: super: {

liboqs =
let inherit (super) stdenv lib fetchFromGitHub cmake ninja
  openssl
; in
stdenv.mkDerivation rec {
  pname = "liboqs";
  version = "0.7.0";
  src = fetchFromGitHub {
    owner = "open-quantum-safe";
    repo = pname;
    rev = version;
    sha256 = "sha256:0i9cagzc1bsshccz3hb3sh3i3b5yj7k337dmflwf933lklxwf3pp";
  };

  dontFixCmake = true;

  nativeBuildInputs = [ cmake ninja ];

  buildInputs = [ openssl ];

  meta = {
    license = lib.licenses.mit;
    hompage = "https://openquantumsafe.org/";
    description = "An open source C library for quantum-resistant cryptographic algorithms";
  };
};

}
