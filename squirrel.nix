super: self: {

squirrel =
let inherit (super) lib stdenv fetchFromGitHub alt-ergo; in
let ocamlPackages = super.ocaml-ng.ocamlPackages_4_14; in
let inherit (ocamlPackages) ocaml dune_3 findlib
  alcotest fmt menhir menhirLib ocamlgraph ocaml_pcre ppx_inline_test; in

let why3 = (super.why3.override { ocamlPackages = ocamlPackages; }).overrideAttrs (o: rec {
  version = "1.5.1";
   src = super.fetchurl {
     url = "https://why3.gitlabpages.inria.fr/releases/${o.pname}-${version}.tar.gz";
     hash = "sha256-vNR7WeiSvg+763GcovoZBFDfncekJMeqNegP4fVw06I=";
   };

}); in

let rev = "0c80cd19918e3652436dca09601b9faaeda39c22"; in
let date = "2023-03-15"; in

let short = lib.substring 0 8 rev; in

stdenv.mkDerivation rec {
  pname = "squirrel";
  version = "dev-${date}-${short}";
  src = fetchFromGitHub {
    owner = "squirrel-prover";
    repo = "squirrel-prover";
    inherit rev;
    hash = "sha256-0JFTdNP5mSvZ9v27r5Vrkb15CvZRENfreasP67DdIZc=";
  };

  buildInputs = [
    ocaml findlib dune_3
    alcotest
    alt-ergo
    fmt
    menhir
    menhirLib
    ocamlgraph
    ocaml_pcre
    ppx_inline_test
    why3
  ];

  preConfigure = ''
    sed 's/GITHASH/${rev}/' < src/commit.ml.in > src/commit.ml
  '';

  buildPhase = ''
    runHook preBuild
    dune build squirrel.exe
    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin
    cp -r theories $out/bin/
    cp _build/default/squirrel.exe $out/bin/squirrel
    runHook postInstall
  '';

  meta = {
    description = "An interactive prover for the formal verification of security protocols";
    homepage = "https://squirrel-prover.github.io/";
    license = lib.licenses.gpl3Only;
    inherit (ocaml.meta) platforms;
  };

};

}
