self: super: {

deepsec =
let version = "2.0.1"; in
with super; stdenv.mkDerivation {
  name = "deepsec-${version}";
  src = fetchzip {
    url = "https://github.com/DeepSec-prover/deepsec/archive/v${version}.tar.gz";
    sha256 = "0k7vzfz0phhpan19ws2cdj1aw7a6wdkdxywxp4n6sxvb5hxvl80r";
  };

  buildInputs = with ocamlPackages; [ ocaml findlib ocamlbuild ];

  installPhase = ''
    mkdir -p $out/bin
    for p in deepsec deepsec_worker deepsec_api
    do
      cp $p $out/bin
    done
  '';

  meta = {
    homepage = "https://deepsec-prover.github.io/";
    description = "Deciding Equivalence Properties in Security Protocols";
    license = lib.licenses.gpl3;
    inherit (ocaml.meta) platforms;
  };
};

deepsec-ui =
let version = "1.0.0"; in
with super; appimageTools.wrapType2 {
  name = "deepsec-ui-${version}";
    src = fetchurl {
      url = "https://homepages.loria.fr/VCheval/official_releases/deepsec-ui-1.0.0.AppImage";
      sha256 = "1fc9ddghs1hk96g2cziwi9i6anvcd0zkyh8qx66lmak115igyp7d";
  };

  profile = ''
    export LC_ALL=C.UTF-8
    export XDG_DATA_DIRS=${gsettings-desktop-schemas}/share/gsettings-schemas/${gsettings-desktop-schemas.name}:${gtk3}/share/gsettings-schemas/${gtk3.name}:$XDG_DATA_DIRS
  '';

  extraPkgs = p: [ p.deepsec ];

  inherit (self.deepsec) meta;

};

}
