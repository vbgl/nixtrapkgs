self: super: {
lem =
let inherit (super) stdenv fetchFromGitHub ocamlPackages; in
stdenv.mkDerivation rec {
  version = "2020-06-03";
  pname = "lem";

  src = fetchFromGitHub {
    owner = "rems-project";
    repo = pname;
    rev = version;
    sha256 = "0ff3d603w5ny4pi6l8b2snjbc80zmd5f5c6pvryn7533p3wld3z1";
  };

  makeFlags = [ "INSTALL_DIR=$(out)" ];

  buildInputs = with ocamlPackages; [ ocaml findlib ocamlbuild num zarith ];

  createFindlibDestdir = true;

  meta = {
    description = "Semantic definition language";
    inherit (src.meta) homepage;
    inherit (ocamlPackages.ocaml.meta) platforms;
  };
  
};

linksem =
let inherit (super) stdenv lib fetchFromGitHub ocamlPackages; in
stdenv.mkDerivation rec {
  version = "0.7";
  pname = "linksem";

  src = fetchFromGitHub {
    owner = "rems-project";
    repo = pname;
    rev = version;
    sha256 = "104imvn7h8m2k4z31b5g1i8rg0prvx2jwb006bdym39j0iviq2yr";
  };

  buildInputs = with ocamlPackages; [ ocaml findlib ocamlbuild zarith ];

  propagatedBuildInputs = [ self.lem ocamlPackages.num ];

  createFindlibDestdir = true;

  meta = {
    description = "Semantic model for aspects of ELF static linking and DWARF debug information";
    license = lib.licenses.bsd2;
    inherit (src.meta) homepage;
    inherit (ocamlPackages.ocaml.meta) platforms;
  };
  
};

sail =
let inherit (super) stdenv lib fetchFromGitHub ocamlPackages ott; in
stdenv.mkDerivation rec {
  version = "0.14";
  pname = "sail";

  src = fetchFromGitHub {
    owner = "rems-project";
    repo = pname;
    rev = version;
    sha256 = "14bb90lcqcbssdnrxccwgzsk2imgamqjiyaldkc259d80p96b3ir";
  };

  buildInputs = [ ott self.linksem ] ++ (with ocamlPackages; [
   ocaml findlib ocamlbuild menhir
   base64 linenoise omd pprint yojson zarith ]);

  makeFlags = [ "INSTALL_DIR=$(out)" "SHARE_DIR=$(out)/share" ];

  buildFlags = [ "isail" ];

  createFindlibDestdir = true;

  meta = {
    inherit (src.meta) homepage;
    inherit (ocamlPackages.ocaml.meta) platforms;
    license = lib.licenses.bsd2;
  };

};

}
