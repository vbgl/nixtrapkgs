self: super:

{

fact =
let inherit (super) stdenv lib fetchFromGitHub clang; in
let ocamlPackages = super.ocaml-ng.ocamlPackages; in
let inherit (ocamlPackages) ocaml_oasis ocaml findlib ocamlbuild cppo cppo_ocamlbuild
  ansiterminal core ctypes dolog llvm lwt menhir ppx_deriving z3; in
stdenv.mkDerivation {

  pname = "fact";
  version = "2022-02-18";

  src = fetchFromGitHub {
    owner = "PLSysSec";
    repo = "FaCT";
    rev = "b3daa03f48447285117f426129dbf539a7f32f4a";
    sha256 = "sha256:1sfii1nxa63jaiqkmq362pn2c2bnj4h4s12zkn6vncq8nnnsbpqk";
  };

  preConfigure = ''
    substituteInPlace src/command_util.ml --replace clang-6.0 ${clang}/bin/clang
    oasis setup
  '';

  nativeBuildInputs = [ cppo ocaml_oasis ];
  buildInputs = [
    ocaml findlib
    ocamlbuild
    cppo_ocamlbuild
    ansiterminal
    core
    ctypes
    dolog
    llvm
    lwt
    menhir
    ppx_deriving
    z3
    super.z3
  ];

  dontStrip = true;

  meta = {
    description = "Compiler for the Flexible and Constant Time cryptographic programming language";
    homepage = "https://fact.programming.systems/";
    license = lib.licenses.bsd3;
    platforms = lib.platforms.unix;
    maintainers = [ lib.maintainers.vbgl ];
  };


};

}
