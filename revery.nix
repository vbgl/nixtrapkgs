self: super: {

reason-native =
let
  version = "0.0.0";
  inherit (super) fetchFromGitHub;
  inherit (super.ocamlPackages) buildDunePackage reason re;
  src = fetchFromGitHub {
    owner = "facebookexperimental";
    repo = "reason-native";
    rev = "a33f1528a6dd86c67f365e226c81312733181c87";
    sha256 = "0xlsz38p2hixpva7ihgx97cfmzxsf9zpfn5b9fmycygihf6jkf0x";
  };
in rec {

cli =
buildDunePackage {
  inherit version src;
  pname = "cli";
  buildInputs = [ reason ];
  propagatedBuildInputs = [ pastel ];
};

console =
buildDunePackage {
  inherit version src;
  pname = "console";
  buildInputs = [ reason ];
};

file-context-printer =
buildDunePackage {
  inherit version src;
  pname = "file-context-printer";
  buildInputs = [ reason ];
  propagatedBuildInputs = [ pastel ];
};

pastel =
buildDunePackage {
  inherit version src;
  pname = "pastel";
  buildInputs = [ reason ];
  propagatedBuildInputs = [ re ];
};

rely =
buildDunePackage {
  inherit version src;
  pname = "rely";
  buildInputs = [ reason ];
  propagatedBuildInputs = [ cli file-context-printer ];
};

};

reglm =
let inherit (super) stdenv fetchFromGitHub reason glm dune ocamlPackages; in
let inherit (ocamlPackages) ocaml findlib; in
stdenv.mkDerivation rec {
  version = "0.2.0";
  name = "reglm-${version}";
  src = fetchFromGitHub {
    owner = "bryphe";
    repo = "reason-gl-matrix";
    rev = "e9f6bfa99524ae7fc8082baaa6087af520d0c901";
    sha256 = "02j4ha6b08kzxkg75mwgh5qq9f2gby3czaxwh086a9hpr7ls97wi";
  };

  patches = [ ./revery.patch ];

  postPatch = "substituteInPlace src/glm_stubs.cpp --replace glm.hpp glm/glm.hpp --replace gtc glm/gtc";

  nativeBuildInputs = [ reason ];

  buildInputs = [ ocaml findlib dune glm ];

  buildPhase = "dune build";

  inherit (dune) installPhase;

  meta = {
  };
};

reglfw =
let inherit (super) stdenv darwin fetchFromGitHub libGLU reason dune glfw ocamlPackages; in
let inherit (super.xorg) libX11 libXxf86vm libXrandr libXi libXcursor libXinerama; in
let inherit (ocamlPackages) ocaml findlib ppx_tools_versioned lwt_ppx js_of_ocaml ocaml_lwt; in
stdenv.mkDerivation rec {
  version = "3.2.1029";
  name = "reason-glfw-${version}";
  src = fetchFromGitHub {
    owner = "bryphe";
    repo = "reason-glfw";
    rev = "c6a72a9a270864c5e461bb2a602454e9448ed8ea";
    sha256 = "13dpj7xialrwa1lngxg0pdrqx147hmkrscklq7kqp5hmf256k4s2";
  };

  postPatch = ''
    substituteInPlace config/discover.ml --replace lglfw3 lglfw
  '';

  buildInputs = [ js_of_ocaml ocaml reason findlib dune ppx_tools_versioned lwt_ppx ];
  propagatedBuildInputs = [ ocaml_lwt self.reglm glfw ]
  ++ stdenv.lib.optionals stdenv.isLinux [ libGLU libX11 libXi libXxf86vm libXrandr libXcursor  libXinerama ]
  ++ stdenv.lib.optionals stdenv.isDarwin (with darwin.apple_sdk.frameworks; [
    OpenGL Cocoa IOKit CoreVideo ])
  ;

  preConfigure = ''
    export cur__root=$PWD
    export GLFW_INCLUDE_PATH=${glfw}/include/GLFW
    export GLFW_LIB_PATH=${glfw}/lib
  '';

  buildPhase = "dune build";

  inherit (dune) installPhase;

};

ocaml-flex =
let inherit (super) fetchFromGitHub reason ocamlPackages; in
let inherit (ocamlPackages) buildDunePackage ; in
buildDunePackage rec {
  version = "1.2.3";
  pname = "flex";
  useDune2 = true;
  src = fetchFromGitHub {
    owner = "jordwalke";
    repo = "flex";
    rev = "61062e2ea063bbdd9088d1e3887825711af6ddd5";
    sha256 = "1i89ma1vcqla8w7hw4gmc4yfwa864bkwzhbrgicb8i8nijs6b1vi";
  };

  buildInputs = [ reason ];

};

ocaml-fontkit =
let inherit (super) stdenv fetchFromGitHub reason dune ocamlPackages freetype harfbuzz; in
let inherit (ocamlPackages) ocaml findlib lwt_ppx; in
stdenv.mkDerivation rec {
  version = "2.7.0";
  name = "ocaml${ocaml.version}-fontkit-${version}";
  src = fetchFromGitHub {
    owner = "bryphe";
    repo = "reason-fontkit";
    rev = "41d2c3bc8dc62a0c23155646f704b81fac54a049";
    sha256 = "1gnzhzf40hms1qwam12hlywvcq1v5y858qpb5wfyzv3cjs2zdmwa";
  };

  buildInputs = [ ocaml reason findlib dune lwt_ppx freetype harfbuzz ];

  propagatedBuildInputs = [ self.reglfw ];

  buildPhase = ''
    export FREETYPE2_INCLUDE_PATH=${freetype.dev}/include/freetype2
    export HARFBUZZ_INCLUDE_PATH=${harfbuzz.dev}/include/harfbuzz
    export FREETYPE2_LIB_PATH=${freetype}/lib
    export HARFBUZZ_LIB_PATH=${harfbuzz}/lib
    dune build
  '';

  inherit (dune) installPhase;

  meta.broken = false;
};

reason-sdl2 =
let inherit (super) stdenv darwin fetchFromGitHub libGLU SDL2 ocamlPackages; in
let inherit (super.xorg) libX11 libXxf86vm libXrandr libXi libXcursor libXinerama; in
let inherit (ocamlPackages) buildDunePackage js_of_ocaml reason ocaml_lwt lwt_ppx; in
buildDunePackage rec {
  pname = "sdl2";
  version = "2.10.3008";
  src = fetchFromGitHub {
    owner = "revery-ui";
    repo = "reason-sdl2";
    rev = "73dbb08b651d3fe599c9c0feb3e31cfc9ecb1d44";
    sha256 = "02nfwp8dp272rxl5zl3q2vmyd1n347gllinmr1s3a8kmwmpfjgnd";
  };

  preConfigure = ''
    export cur__root=$PWD
    export SDL2_INCLUDE_PATH=${SDL2.dev}/include
    export SDL2_LIB_PATH=${SDL2}/lib
    rm -rf bin_js
  '';

  buildInputs = [ js_of_ocaml reason lwt_ppx ]
  ++ stdenv.lib.optionals stdenv.isLinux [ libGLU libX11 libXi libXxf86vm libXrandr libXcursor  libXinerama ]
  ++ stdenv.lib.optionals stdenv.isDarwin (with darwin.apple_sdk.frameworks; [
    OpenGL Cocoa IOKit CoreVideo ])
  ;
  propagatedBuildInputs = [ ocaml_lwt self.reglm ];

};

reason-font-manager =
let inherit (super) fetchFromGitHub ocamlPackages fontconfig; in
let inherit (ocamlPackages) buildDunePackage reason; in
buildDunePackage rec {
  pname = "reason-font-manager";
  version = "2.0.1";
  src = fetchFromGitHub {
    owner = "revery-ui";
    repo = "reason-font-manager";
    rev = "21050a0e7b58aa4da61b08f937a049a039cd20a2";
    sha256 = "043l04klcxg3hljf85kglrcn4dgcssrlcznqxw849ak9z22myck0";
  };

  preConfigure = ''
    export cur__root=$PWD
  '';

  buildInputs = [ reason fontconfig ];
};

brisk-reconciler =
let inherit (super) fetchFromGitHub ocamlPackages; in
let inherit (ocamlPackages) buildDunePackage reason ppxlib; in
buildDunePackage rec {
  pname = "brisk-reconciler";
  version = "1.0.0-alpha.0";
  src = fetchFromGitHub {
    owner = "briskml";
    repo = "brisk-reconciler";
    rev = "10cab2dd57987473a1190dc2fb0d24f3318dec27";
    sha256 = "0qhfa23hyhh8i955m2wfg2790rr6xpp350yy8nkigcl203ia5gwk";
  };

  buildInputs = [ reason ppxlib ];
};

rebez =
let inherit (super) fetchFromGitHub ocamlPackages; in
let inherit (ocamlPackages) buildDunePackage reason; in
buildDunePackage rec {
  pname = "rebez";
  version = "0.0.1";
  src = fetchFromGitHub {
    owner = "jchavarri";
    repo = "rebez";
    rev = "03fa3b707abb28fdd710eb9e57ba40d9cd6ae163";
    sha256 = "15lj9zb0ys0nljrhkyji0cbgmrn8229j2bzh2bzn7yhnvk1545lj";
  };

  buildInputs = [ reason ];
};

Rench =
let inherit (super) fetchFromGitHub ocamlPackages; in
let inherit (ocamlPackages) buildDunePackage reason fpath ocaml_lwt; in
buildDunePackage rec {
  pname = "Rench";
  version = "1.9.0";
  src = fetchFromGitHub {
    owner = "revery-ui";
    repo = "rench";
    rev = "45adffda5fabc02ca6b4cada211d24f0516769bc";
    sha256 = "0sms8r0fc73f4wnq6kwb6i7lf4va2as2am9ri5xxfgb8iq4jpnh2";
  };

  buildInputs = [ reason ];
  propagatedBuildInputs = [ fpath ocaml_lwt self.reason-native.console ];
};

reperf =
let inherit (super) fetchFromGitHub ocamlPackages; in
let inherit (ocamlPackages) buildDunePackage reason printbox; in
buildDunePackage rec {
  pname = "reperf";
  version = "1.4.0";
  src = fetchFromGitHub {
    owner = "bryphe";
    repo = "reperf";
    rev = "d9f5925f42e0e67284c1ecfa8328bfab2eff7399";
    sha256 = "0wrqb0996lj0d1sb6qb8d5m546idfbajzvpk1ipd8ca514fcjbvr";
  };

  buildInputs = [ reason ];
  propagatedBuildInputs = [ self.reason-native.pastel printbox ];
};

Revery =
let inherit (super) stdenv fetchFromGitHub pkg-config bzip2 libpng ocamlPackages gtk3; in
let inherit (ocamlPackages) buildDunePackage reason js_of_ocaml ppx_tools_versioned lwt_ppx; in
buildDunePackage rec {
  version = "0.28.0";
  pname = "Revery";
  src = fetchFromGitHub {
    owner = "revery-ui";
    repo = "revery";
    rev = "d22d77d60e1923437fed4b7099a6ed80840e70b9";
    sha256 = "0wapwgg90y1fhakas5c42c7nj1lr1aidpl4ffcckabhmpwlfv7i8";
  };

  hardeningDisable = [ "format" ];

  nativeBuildInputs = [ pkg-config ];
  buildInputs = [ gtk3 reason ppx_tools_versioned lwt_ppx js_of_ocaml bzip2 libpng ];

  propagatedBuildInputs = [ self.brisk-reconciler self.ocaml-flex self.ocaml-fontkit self.reactify self.reason-font-manager self.reason-sdl2 self.reason-native.console self.reason-native.rely self.rebez self.Rench self.reperf ];

  meta = {
    broken = false;
  };
};

}
