self: super: {

lite-xl =
let inherit (super) stdenv lib fetchFromGitHub meson pkg-config ninja
  pcre2 SDL2 reproc freetype agg; in
let lua = super.lua5_4; in
stdenv.mkDerivation rec {
  pname = "lite-xl";
  version = "2.1.7";
  src = fetchFromGitHub {
    owner = pname;
    repo = pname;
    rev = "v${version}";
    hash = "sha256-Ig0XDxnll/zruAwWHwuXiqumBXgAPxuK0E1ELupvcXo=";
  };

  mesonFlags = [
    "-Duse_system_lua=true"
  ];

  nativeBuildInputs = [ meson pkg-config ninja ];
  buildInputs = [ lua pcre2 SDL2 reproc freetype agg ];
};

}
