self: super:

{

isabelle2018 =
  let inherit (super) isabelle polyml57 fetchurl perlPackages
; in
(isabelle.override {
  polyml = polyml57.overrideAttrs (_: {
      configureFlags = [ "--enable-intinf-as-int" "--with-gmp" "--disable-shared" ];
    });
}).overrideAttrs (o: rec {
  name = "isabelle-2018";
  dirname = "Isabelle2018";
  sourceRoot = dirname;
  src = fetchurl {
    url = "https://isabelle.in.tum.de/website-${dirname}/dist/${dirname}_linux.tar.gz";
    sha256 = "1928lwrw1v1p9s23kix30ncpqm8djmrnjixj82f3ni2a8sc3hrsp";
  };

  installPhase = o.installPhase + ''
    substituteInPlace $out/${dirname}/lib/Tools/java --replace '"-Djava.ext.dirs=$(platform_path "$ISABELLE_JDK_HOME/jre/lib/ext")"' ""
  '';
  # substituteInPlace $out/${dirname}/bin/isabelle_java --replace '"-Djava.ext.dirs=$JAVA_HOME/lib/ext"' ""

});

}
