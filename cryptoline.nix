self: super: {

cryptoline =
let inherit (super) stdenv lib fetchFromGitHub ocamlPackages bc; in
stdenv.mkDerivation {
  pname = "cryptoline";
  version = "2022-09-16";

  src = fetchFromGitHub {
    owner = "fmlab-iis";
    repo = "cryptoline";
    rev = "64ee4de3dcf33e225cddb205118a6cb6c38c5c40";
    sha256 = "sha256-FE8/sLEgFRt1yN6Qy58FY4qYE7k/Ysmd/+wQI6cXsqA=";
  };

  nativeBuildInputs = [
    bc
  ];

  buildInputs = [
  ] ++
  (with ocamlPackages; [
    dune_2 ocaml findlib
    lwt
    lwt_ppx
    zarith
  ])
  ;

  dontConfigure = true;

  buildPhase = ''
    runHook preBuild
    patchShebangs aig/*.sh
    dune build -p cryptoline
    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall
    dune install --prefix $out --libdir $OCAMLFIND_DESTDIR
    runHook postInstall
  '';

  meta = {
    description = "Verification of low-level implementations of mathematical constructs";
    homepage = "https://github.com/fmlab-iis/cryptoline";
    license = lib.licenses.mit;
    mainProgram = "cv";
  };

};

}
